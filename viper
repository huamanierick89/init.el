(setq viper-inhibit-startup-message 't)
(setq viper-expert-level '5)
(setq viper-ex-style-editing nil)
(setq viper-ex-style-motion nil)
(define-key viper-insert-basic-map (kbd "C-d") 'delete-char)
(define-key viper-insert-basic-map (kbd "C-c s") 'viper-query-replace)
(define-key viper-insert-basic-map (kbd "C-w") 'kill-region)
(define-key viper-vi-basic-map (kbd "s") 'save-buffer)
(define-key viper-vi-basic-map (kbd "r") 'set-mark-command)
(define-key viper-vi-basic-map (kbd "z z") 'kill-buffer)
(define-key viper-vi-basic-map (kbd "8") 'viper-goto-eol)
(define-key viper-vi-basic-map (kbd "3") 'viper-beginning-of-line)
(define-key viper-vi-basic-map (kbd "C-b") nil)
(define-key viper-vi-basic-map (kbd "2") 'viper-scroll-up-one)
(define-key viper-vi-basic-map (kbd "9") 'viper-scroll-down-one)
;; (define-key viper-vi-basic-map (kbd "q") 'viper-query-replace)
(define-key viper-vi-basic-map (kbd "q") 'switch-to-buffer)
(define-key viper-vi-basic-map (kbd "t") 'viper-save-kill-buffer)
(define-key viper-vi-basic-map (kbd "C-f") 'dired)
(define-key viper-vi-basic-map (kbd "u") 'viper-window-top)
(define-key viper-vi-basic-map (kbd "f") 'viper-window-middle)
(define-key viper-vi-basic-map (kbd "m") 'viper-window-bottom)
(define-key viper-vi-basic-map (kbd "<") 'text-scale-increase)
(define-key viper-vi-basic-map (kbd "G") 'text-scale-decrease)
(define-key viper-vi-basic-map (kbd "g") 'ace-window)
(define-key viper-vi-basic-map (kbd ",") 'undo-tree-visualize)
(define-key viper-vi-basic-map (kbd ".") 'comment-line)
(define-key viper-vi-basic-map (kbd "+") 'enlarge-window-horizontally)
(define-key viper-vi-basic-map (kbd "-") 'shrink-window-horizontally)
(define-key viper-vi-basic-map (kbd "C-e") 'speedbar)
(define-key viper-vi-basic-map (kbd "C-v") 'list-colors-display)
