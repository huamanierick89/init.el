(defun move-text-internal (arg)
  (cond
   ((and mark-active transient-mark-mode)
	(if (> (point) (mark))
		(exchange-point-and-mark))
    (let ((column (current-column))
          (text (delete-and-extract-region (point) (mark))))
      (forward-line arg)
      (move-to-column column t)
      (set-mark (point))
      (insert text)
      (exchange-point-and-mark)
      (setq deactivate-mark nil)))
   (t
    (let ((column (current-column)))
      (beginning-of-line)
      (when (or (> arg 0) (not (bobp)))
        (forward-line)
		(when (or (< arg 0) (not (eobp)))
          (transpose-lines arg))
        (forward-line -1))
      (move-to-column column t)))))
(defun move-text-down (arg)
  "Move region (transient-mark-mode active) or current line
  arg lines down."
  (interactive "*p")
  (move-text-internal arg))
(defun move-text-up (arg)
  "Move region (transient-mark-mode active) or current line
  arg lines up."
  (interactive "*p")
  (move-text-internal (- arg)))
(defun indent-buffer ()
  "Indent the currently visited buffer."
  (interactive)
  (indent-region (point-min) (point-max)))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes (quote (erick)))
 '(custom-safe-themes
   (quote
	("dd565fd01ddf107c5094e05d6c62e398acc2b92bbd706048d2f24003fb29feab" "e74a8c90fda5b7e7bc9d4e10609962b6e82e6d142b6d44f17f5c65d21e3354fc" "5c065698f94832990e00a40abe6311b259b7426e6bc098cb3c573486ef89cf20" "b2cc1b479db77732aeb2e2d02170be3cbaa2c6d4f98f38b57156d2e53fa22c4b" "b68afab2f24d7dc82fe62e89bbc0eef06d1c7a6c79b2e41b295e1ae998836303" "a8a4e94f152df46b8af9b4df7fc7235f92a05a3b22c7f176a71c13b1b2fe4169" "b90cf3e32e50b3af255f56bf56efc44f747d05ef81bdbee6c25e0a445417f44b" "4e632b1b12bca47068eb52ebfb6f9f376868bbd5965f3e4c22ca0ffa29b40995" "d27916d672fd99d343923e46e15f74ef77ed378cf08af7e4ee5d6c30b000edeb" "1ff99c0c1d9b21111a7f00a12f8261fcf7461fa2ddcf518fdf063e777d657534" "62e5b6b5e25a88976fc1a63220a3d168973735dbebd1f315efd6631f9534b4c6" "653109cf22a741fa7e2933a587fe49fbf7939e445de90364386526014e0a86f3" "4a8c3ff997879794f18fef3ac5420b1c2f7eebd2860affa238551ed4b5ccfec0" "8572bf1f15b58dfff3b52ee781235952431e91667054e12a9a91a492774be523" default)))
 '(delete-selection-mode t)
 '(display-battery-mode t)
 '(electric-pair-mode t)
 '(electric-pair-pairs (quote ((39 . 39) (96 . 96))))
 '(font-lock-verbose 3)
 '(fringe-mode (quote (1 . 1)) nil (fringe))
 '(global-auto-revert-mode t)
 '(global-display-line-numbers-mode t)
 '(global-linum-mode nil)
 '(global-whitespace-mode t)
 '(inhibit-startup-screen t)
 '(line-number-mode nil)
 '(make-backup-files nil)
 '(menu-bar-mode nil)
 '(mode-line-format
   (quote
	("%e" mode-line-front-space mode-line-mule-info mode-line-client mode-line-modified mode-line-remote mode-line-frame-identification mode-line-buffer-identification
	 (vc-mode vc-mode)
	 " " mode-line-misc-info mode-line-end-spaces mode-line-modes)))
 '(package-selected-packages
   (quote
	(auto-complete yasnippet undo-tree rust-mode yafolding multiple-cursors org-bullets rjsx-mode use-package ace-window)))
 '(scroll-bar-mode nil)
 '(sgml-basic-offset 4)
 '(show-paren-mode t)
 '(show-trailing-whitespace t)
 '(speedbar-indentation-width 3)
 '(speedbar-show-unknown-files t)
 '(tab-width 4)
 '(tool-bar-mode nil)
 '(truncate-lines t)
 '(whitespace-style
   (quote
	(trailing tabs indentation::space indentation tab-mark))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(aw-leading-char-face ((t (:foreground "cyan" :weight bold :height 3.0))))
 '(js2-function-call ((t (:foreground "#89DDFF" :slant italic))))
 '(js2-function-param ((t (:foreground "#F78C6C" :weight bold))))
 '(line-number-current-line ((t (:background "red" :foreground "black"))))
 '(my-linum-hl ((t (:inherit linum :background "red"))))
 '(show-paren-match ((t (:foreground "green" :underline t :weight bold))))
 '(vertical-border ((t (:foreground "dark violet"))))
 '(viper-minibuffer-insert ((t (:foreground "dark gray" :weight bold))))
 '(whitespace-indentation ((t (:foreground "firebrick"))))
 '(whitespace-space ((t (:foreground "red"))))
 '(whitespace-space-before-tab ((t (:foreground "red"))))
 '(whitespace-tab ((t (:foreground "darkgray")))))

(global-set-key (kbd "M-p") 'previous-line)
(global-set-key (kbd "M-n") 'next-line)
(global-set-key (kbd "M--") 'shrink-window-horizontally)
(global-set-key (kbd "M-+") 'enlarge-window-horizontally)
(global-set-key (kbd "C-x f") 'delete-frame)
(global-set-key (kbd "M-j")  'move-text-down)
(global-set-key (kbd "M-k")  'move-text-up)
(global-set-key (kbd "M-o") 'enlarge-window)
(global-set-key (kbd "M-ñ") 'shrink-window)
(global-set-key (kbd "M--") 'shrink-window-horizontally)
(global-set-key (kbd "M-+") 'enlarge-window-horizontally)
(global-set-key (kbd "M-m") 'viper-intercept-ESC-key)
(global-set-key (kbd "C-c e") 'speedbar)
(global-set-key (kbd "C-c c") 'comment-line)
(global-set-key (kbd "C-c f") 'make-frame)
(global-set-key (kbd "C-c t") 'shell)
(global-set-key (kbd "C-c l") 'list-colors-display)
(global-set-key (kbd "C-c w") 'toggle-frame-fullscreen)
(global-set-key (kbd "M-l") 'recenter-top-bottom)
(global-set-key (kbd "C-c i") 'indent-buffer)
