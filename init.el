;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)

(require 'package)
(let* ((no-ssl (and (memq system-type '(windows-nt ms-dos))
                    (not (gnutls-available-p))))
       (url (concat (if no-ssl "http""https") "://melpa.org/packages/")))
  (add-to-list'package-archives (cons"melpa" url) t))
(when (>= emacs-major-version 24)
  (add-to-list'package-archives '("gnu" . "http://elpa.gnu.org/packages/")))

(setq custom-file "~/.emacs.d/custom.el")
(load custom-file)
(fset 'yes-or-no-p 'y-or-n-p)
(setq history-delete-duplicates t)
(add-to-list 'before-save-hook 'delete-trailing-whitespace)
(use-package viper
  :ensure t
  :init
  (setq viper-mode t))
(use-package ace-window
  :ensure t
  :bind
  ("C-x o" . ace-window)
  :config
  (setq aw-keys '(?a ?s ?d ?f ?g ?h ?j ?k ?l ?ñ))
  (setq aw-background t))

(use-package multiple-cursors
  :ensure t
  :bind
  (("M-," . mc/mark-next-like-this)
   ("M-i" . 'mc/mark-previous-like-this)
   ("C-c z" . 'mc/mark-all-like-this)))
(use-package js2-mode
  :ensure t
  :config
  (add-to-list 'auto-mode-alist '("\\.js\\'" . js2-mode)))
(use-package rjsx-mode
  :ensure t
  :mode ("\\.jsx\\'" . rjsx-mode))
(use-package rust-mode
  :ensure t
  :mode ("\\.rs\\'" . rust-mode))
(use-package yafolding
  :ensure t
  :bind ("C-c k" . yafolding-toggle-element))
(use-package org-bullets
  :ensure t
  :config
  (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1))))
(use-package undo-tree
  :ensure t
  :init
  (global-undo-tree-mode))
(yas-global-mode 1)
(use-package yasnippet
  :demand t
  :diminish yas-minor-mode
  :init
  (setq yas-snippet-dirs '("~/.emacs.d/private/snippets/"))
  :config (yas-reload-all)
  :bind
  ("C-c n" . yas-new-snippet))
(use-package auto-complete
  :ensure t
  :init
  (progn
	(ac-config-default)
	(global-auto-complete-mode t)
	)
  :hook (
		 (rust-mode . auto-complete-mode)
		 (rjsx-mode . auto-complete-mode)
		 ))
